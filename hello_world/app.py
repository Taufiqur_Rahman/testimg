import base64
import json
from io import BytesIO, StringIO

import boto3
from PIL import Image
import PIL.Image



print ('Loading function')


def resize_image(self, image, resize_width, resize_height):

        if resize_width == image.width and resize_height == image.height:
            return image

        original_ratio = image.width / float(image.height)
        resize_ratio = resize_width / float(resize_height)

    
        if original_ratio > resize_ratio:

            resize_height = int(round(resize_width / original_ratio))
        else:
            resize_width = int(round(resize_height * original_ratio))

        if ((image.width - resize_width) + (image.height - resize_height)) < 0:
            filter_name = 'cat'
        else:
            filter_name = 'cat2'

        image.resize(width=resize_width, height=resize_height, filter=filter_name, blur=1)

def response(self):
            return base64.b64encode(bytes(self.image)).decode('utf-8')


def lambda_handler(event, context):
    img = Image.open('cat3.png')
    img.resize_image()  
    img.show()
    

    return{
                "statusCode": 200,
                "body": img.response(),
                "headers": {
                'Content-Type': 'image/png'
        },
            "isBase64Encoded": True
    }