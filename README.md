# Resize Image
----

### Requirements

  - Docker
  - AWS SAM CLI
  - Python 3.6
  - Pip
  - Boto 3

##### AWS SAM CLI
---
Before that, you have to install docker first in Ubuntu 18.04 and running to be able to run serverless projects and functions locally with the AWS SAM CLI. The AWS SAM CLI uses the DOCKER_HOST environment variable to contact the Docker daemon.

Then install aws sam cli 

```
$ pip install awscli aws-sam-cli
```

##### AWS CONFIGURATION
---
```
$aws configure
AWS Access Key ID: 
AWS Secret Access Key:
Default region name: 
Default output format [json]:
```

##### How to deploy
---
- First need to build docker image

```
$ docker build -t deploy_image -f Dockerfile_deploy .
```

- Deploy the docker image

```
$ docker run -it --rm --privileged \
    -v /var/lib/docker \
    -e AWS_ACCESS_KEY_ID=`grep "aws_access_key_id" ~/.aws/credentials | awk '{print $3}'` \
    -e AWS_SECRET_ACCESS_KEY=`grep "aws_secret_access_key" ~/.aws/credentials | awk '{print $3}'` \
    -e AWS_DEFAULT_REGION=`grep "region" ~/.aws/config | awk '{print $3}'` \
    -e bucket_name=${your_bucket_name} \
    deploy_image
```
 - Get the endpoint
Go to cloudformation in your AWS account. A stack has been created already. Click the stack and check the "Outputs". You will find the Endpoint. 
Otherwise write the following command.

```
$ aws cloudformation describe-stacks --stack-name testimg --query 'Stacks[].Outputs' | grep https | sed "s/.*\"\(https.*\)\".*/\1/"
```

 - Resize the image using endpoint
 
```
$ curl -H "Accept: image/png" data-binary "@cat3.png" -X POST https://zflhtdb3zf.execute-api.us-east-2.amazonaws.com/Prod/hello/ -o test.png
```
 
##### Deploy in Ububtu 18.04 locally with docker
---
1. Build docker image
```
$ docker build -t deploy_image -f Dockerfile_test .
```
2. To get endpoint run the following cmd using docker
```
$ docker run -it --rm --privileged \
    -v /var/lib/docker \
    --hostname ovro_container \
    --name ovro_container \
    -e AWS_ACCESS_KEY_ID="" \
    -e AWS_SECRET_ACCESS_KEY="" \
    -e AWS_DEFAULT_REGION=`grep "region" ~/.aws/config | awk '{print $3}'` \
    deploy_image
```
3. Send the original image through endpoint
```
$ docker exec -it ovro_container curl -H "Accept: image/png" -H "Content-Type: image/png" --data-binary "@/testimg/cat3.png" -X POST http://127.0.0.1:3000/src -o test.png
```
4. Copy the resize image.
```
$ docker cp ovro_container:/test.png .
```
5. To check the lambda log.
```
$ sam logs -n HelloWorldFunction --stack-name testimg --tail
```